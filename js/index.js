$(document).ready(function(){

    $("[data-toogle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
      console.log('Modal funcionando', e);
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e) {
      console.log('Modal se mostró', e);
    });

    $('#contacto').on('hide.bs.modal', function (e) {
      console.log('Modal se oculta', e);
    });

    $('#contacto').on('hidden.bs.modal', function (e) {
      console.log('Modal se ocultó', e);
      $('#contactoBtn').prop('disabled', false);
    });

  });